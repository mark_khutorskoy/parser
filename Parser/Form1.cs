﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CsQuery;
using System.IO;

namespace Parser
{
    
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnParse_Click(object sender, EventArgs e)
        {
            List <string> resultItems = new List<string>();
            List<string> resultSet = new List<string>();
            List<string> resultPrice = new List<string>();

            string pathTxt= @"F:\ВЕРНУТЬ НА РАБ СТОЛ\Ланин\Parser\result.txt";
            string pathURL = "http://www.starcitygames.com/results?&lang%5B%5D=1&s%5B0%5D=5369&s%5B1%5D=5377&s%5B2%5D=5304&s%5B3%5D=5352&foil=nofoil&mincost=10&maxcost=9999.99&r%5BRRR3%5D=R&r%5BRRR4%5D=M&r%5BRRR10%5D=P&g%5BG1%5D=NM/M&sort1=4&sort2=1&numpage=25&display=3&startnum=0";

            var web = CQ.CreateFromUrl(pathURL);
            var next_page = web.Select("a");

                int num_page = 1;
                foreach (var k in next_page)
                {
                    if (k.InnerText.Trim() == Convert.ToString(num_page))
                    {

                        num_page++;
                        MessageBox.Show("New page loading...");
                        web = CQ.CreateFromUrl(k["href"]);

                        var card_name = web.Select(".card_popup ");
                        foreach (var kk in card_name)
                            resultItems.Add(Convert.ToString(kk.InnerText));
                        
                            var card_edition = web[".search_results_2"].Contents();
                            foreach (var ed in card_edition)
                                try
                                {
                                    resultSet.Add(Convert.ToString(ed.InnerText));
                                }
                                catch { }
                        
                        var card_price = web.Select(".search_results_9  ");
                        foreach (var pr in card_price)
                            resultPrice.Add(Convert.ToString(pr.InnerText));
                    }
                }

                StreamWriter sw = new StreamWriter(pathTxt);
                {
                    string total = "";
                    for (int i = 0; i < resultItems.Count();i++)
                    {
                        total = resultItems[i] + "| " + resultSet[i]+ "| " + resultPrice[i];
                        sw.WriteLine(total);                       
                    }
                    sw.Close();
                }
        }       
    }
}
